function tabsHandler() {
    console.log("La fonction tabsHandler est lancée");

    var component = $('.accordion1'),//.grillex .accordion1
        componentNav = component.find('.barside'), //.barside
        componentNavItems = componentNav.find('p'),    //p
        componentContents = component.find('.contenu'); //.contenu

    componentNavItems.on('click', function () {
        var targetID = $(this).data('id');

        console.log('Je clique sur ' + targetID);

        if ($('#' + targetID).hasClass('tabs-content--inactive')) {
            console.log('Mon élément est masqué');

            // Gestion de l'affichage du contenu
            componentContents.addClass('tabs-content--inactive');
            $('#' + targetID).removeClass('tabs-content--inactive');

            // Gestion de l'affichage du menu
            componentNavItems.removeClass('tabs-nav-item--active');
            $(this).addClass('tabs-nav-item--active');
        } else {
            console.log("Mon élément est visible");
        }

    });
}

$(document).ready(function () {
    var tabsComponent = $('.accordion1');
    console.log('Le DOM est prêt');

    if (tabsComponent.length > 0) {
        console.log("Mon composant tabs est bien présent");
        tabsHandler();
    } else {
        console.log("Mon composant tabs n'est pas chargé");
    }
});